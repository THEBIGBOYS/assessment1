from person import Person
fileref = open("smallData.csv", "r")
# Algorithms credit: https://kiariela.tk/software_development_project/python_portfolio/python_portfolio.xhtml
lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)

    people.append(person)

people_dict = {}
for person in people:
    people_dict[hash(person)] = person

for k, v in people_dict.items():
    print(k, ":", v)

