import signal
import os
import time
import sys

def readConfiguration(signalNumber, frame):
    print('(SIGHUP) reading configuration')

    return

def terminateProcess(signalNumber, frame):
    print('(SIGTERM) terminating the process')
    sys.exit()

def recieveSignal(signalNumber, frame):
    print('Received', signalNumber)
    return

if __name__ == '__main__':
    signal.signal(signal.SIGHUP, readConfiguration)
    signal.signal(signal.SIGINT, recieveSignal)
    signal.signal(signal.SIGQUIT, recieveSignal)
    signal.signal(signal.SIGILL, recieveSignal)
    signal.signal(signal.SIGTRAP, recieveSignal)
    signal.signal(signal.SIGABRT, recieveSignal)
    signal.signal(signal.SIGBUS, recieveSignal)
    signal.signal(signal.SIGFPE, recieveSignal)

    signal.signal(signal.SIGUSR1, recieveSignal)
    signal.signal(signal.SIGSEGV, recieveSignal)
    signal.signal(signal.SIGUSR2, recieveSignal)
    signal.signal(signal.SIGPIPE, recieveSignal)
    signal.signal(signal.SIGALRM, recieveSignal)
    signal.signal(signal.SIGTERM, terminateProcess)

    print("My PID is:", os.getpid())

    while True:
        print('waiting...')
        time.sleep(3)
