from person import Person
fileref = open("smallData.csv", "r")
# Algorithms credit: https://kiariela.tk/software_development_project/python_portfolio/python_portfolio.xhtml
lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)

    people.append(person)

def linearSearch(people, searchString):
    for i in range(len(people)):
        if people[i].surname == searchString:
            return print(people[i])
    return -1

linearSearch(people, 'Pezzini')

def binarySearch(people, searchString):
    first = 0
    last = len(people) - 1
    index = -1

    while (first <= last) and (index == -1):
        mid = (first + last)//2
        if people[mid].surname == searchString:
            index = mid
        else:
            if searchString < people[mid].surname:
                last = mid - 1
            else:
                first = mid + 1
    return print(people[index])

binarySearch(people, "Pennyworth")


# Source:  https://stackoverflow.com/a/46931787
def jumpSearch(people, firstname):
    low = 0
    interval = int(math.sqrt(len(people)))
    for i in range(0, len(people), interval):
        if people[i].name < firstname:
            low = i
        elif people[i].name == firstname:
            return print(people[i])
        else:
            break # bigger number is found
    c=low
    for j in people[low:]:
        if j.name==firstname:
            return print(people[c])
        c+=1
    return "Not found"

jumpSearch(people, "Alfred")
