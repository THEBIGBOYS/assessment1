from person import *
# Algorithms credit: https://kiariela.tk/software_development_project/python_portfolio/python_portfolio.xhtml

# fileref = open('smallData.dat', 'r')
fileref = open('mediumData.dat', 'r')
# fileref = open('largeData.dat', 'r')

lines = fileref.readlines()

people = []

storage = ""
for line in lines:
    if line == "\n":
        people.append(FromString(storage))
        storage = ""
    else:
        storage += line

#for _person in people:
    #_person.print()

import math

def swap(index1, index2, people):
    if (people[index1].name > people[index2].name):
        people[index1], people[index2] = people[index2], people[index1]

        return True
    return False

input = [ 5, 3, 6, 5, 10, 200, 50, 7, 5, 4, 1]

def bubble_sort(input):
    hasSwapped = True
    while (hasSwapped):
        hasSwapped = False
        for index in range(0, len(input) - 1):
            if (swap(index, index + 1, input)):
                hasSwapped = True

#bubble_sort(input)

def pythonSort(people):
    people.sort(key=lambda person: person.surname.upper(), reverse=False)

#print(input)

def mergesort( a ):
    n = len(a)
    if ( n == 1 ):
        return a

    l1 = a[0 : math.ceil(n/2)]
    l2 = a[math.ceil(n/2) : n]

    l1 = mergesort( l1 )
    l2 = mergesort( l2 )

    return merge( l1, l2 )

def merge( a, b ):

    c = []
    while ( len(a) > 0 and len(b) > 0 ):
        if ( a[0].name > b[0].name ):
            c.append(b.pop(0))
        else:
            c.append(a.pop(0))

    while ( len(a) > 0 ):
        c.append(a.pop(0))

    while ( len(b) > 0 ):
        c.append(b.pop(0))


    return c

def partition(people, low, high):
    i = ( low - 1 )
    pivot = people[high].name

    for j in range(low, high):

        if people[j].name <= pivot:
            i += 1
            people[i], people[j] = people[j], people[i]

    people[i+1], people[high] = people[high], people[i+1]
    return (i + 1)

low = 0
high = len(people) - 1

def quicksort(people, low, high):
    if low < high:
        pi = partition(people, low, high)

        quicksort(people, low, pi - 1)
        quicksort(people, pi+1, high)

#quicksort(input, low, high)

import time
# start = time.perf_counter()
# bubble_sort(people)
# end = time.perf_counter()
# bubblesorttime = end - start
# print("Bubble sort time:\t\t" + str(bubblesorttime))
#
# start = time.perf_counter()
# mergesort(people)
# end = time.perf_counter()
# mergesorttime = end - start
# print("Merge sort time:\t\t" + str(mergesorttime))
#
# start = time.perf_counter()
# quicksort(people, low, high)
# end = time.perf_counter()
# quicksorttime = end - start
# print("Quick sort time:\t\t" + str(quicksorttime))
#
# start = time.perf_counter()
# pythonSort(people)
# end = time.perf_counter()
# pythonsorttime = end - start
# print("Python sort time:\t\t" + str(pythonsorttime))

for person in people:
    print(person)

# outfile = open('smallData.csv', 'w')
outfile = open('mediumData.csv', 'w')
# outfile = open('largeData.csv', 'w')

for person in people:
    outfile.write(str(person) + '\n')
