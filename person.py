class Person():
    def __init__(self, firstname, lastname, street, city, state, postcode):
        self.name = firstname
        self.surname = lastname
        self.street = street
        self.city = city
        self.state = state
        self.postcode = postcode

    def print(self):
        print("{} {} {} {} {} {}".format(self.name, self.surname, self.street, self.city, self.state, self.postcode))

    def __str__(self):
        return "{}, {}, {}, {}, {}, {}".format(self.name, self.surname, self.street, self.city, self.state, self.postcode)

def FromStrings(_fullname, _address1, _address2):
    fullname = _fullname.split(", ")
    name = fullname[1]
    surname = fullname[0]

    street = _address1
    address2 = _address2.split("\t")

    city = address2[0]
    state = address2[1].split(' ')[0]

    postcode = address2[2]

    return Person(name, surname, street, city, state, postcode)


def FromString(raw):
    lines = raw.split('\n')

    return FromStrings(lines[0], lines[1], lines[2])


