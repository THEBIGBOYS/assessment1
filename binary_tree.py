from person import Person
fileref = open("smallData.csv", "r")
# Algorithms credit: https://kiariela.tk/software_development_project/python_portfolio/python_portfolio.xhtml
lines = fileref.readlines()

firstname = ''
lastname = ''
street = ''
city = ''
state = ''
postcode = ''
people = []

for line in lines:
    firstname, lastname, street, city, state, postcode = line.strip().split(', ')
    person = Person(firstname, lastname, street, city, state, postcode)

    people.append(person)

class Node:

    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        if self.data:
            if data.name < self.data.name:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            elif data.name > self.data.name:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    def PrintTree(self):
        if self.left:
            self.left.PrintTree()
        print(self.data)
        if self.right:
            self.right.PrintTree()

root = Node(people[0])
for person in people:
    root.insert(person)

root.insert(Person("harley", 'calvert', 'Pickle Anaconda St', 'Franston', 'THICC', '3199'))
root.PrintTree()
